import Fibonacci.fib

object Main extends App {
  val n = 11
  val f0 = fib(n - 1)
  val f1 = fib(n)
  printf("%d-th/%d-th Fibonacci numbers are, %d and %d.", n-1, n, f0, f1)
}
