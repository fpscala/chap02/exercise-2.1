object Fibonacci {
  def fib(n: Int): Int = {
    def go(n: Int, x: Int, y: Int): Int = {
      if (n <= 0)
        0
      else if (n == 1)
        y
      else
        go(n - 1, y, x + y)
    }
    go(n, 0, 1)
  }
}
